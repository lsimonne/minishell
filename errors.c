/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/10 13:22:07 by lsimonne          #+#    #+#             */
/*   Updated: 2016/03/25 15:12:13 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

int		ft_wrong_command(int code, t_command cmd)
{
	if (cmd.name)
	{
		ft_putstr("minishell: ");
		ft_putstr_fd(cmd.name, 2);
		if (code == -1)
			ft_putendl_fd(": command not found", 2);
		else
			ft_putendl_fd(": Permission denied", 2);
	}
	return (-1);
}

int		ft_cd_error(int code, char *path)
{
	ft_putstr_fd("minishell: cd: ", 2);
	if (code == 1)
		ft_putendl_fd("HOME not set", 2);
	else if (code == 2)
		ft_putendl_fd("OLDPWD not set", 2);
	else if (code == 3)
	{
		ft_putchar(*path);
		ft_putendl(": invalid option\ncd: usage: cd [-LP] [dir]");
	}
	else if (code == 4)
		ft_putendl_fd("Too many arguments", 2);
	else
	{
		if (access(path, F_OK) == -1)
			ft_putstr_fd("No such file or directory: ", 2);
		else if (access(path, R_OK) == -1)
			ft_putstr_fd("Permission denied: ", 2);
		ft_putendl_fd(path, 2);
		ft_strdel(&path);
	}
	return (-1);
}

int		ft_arg_error(int code, char *arg)
{
	ft_putstr_fd("minishell: ", 2);
	ft_putstr_fd(arg, 2);
	if (code == 1)
		ft_putendl_fd(": No such file or directory", 2);
	else if (code == 2)
		ft_putendl_fd(": Permission denied", 2);
	else if (code == 3)
		ft_putendl_fd(": is a directory", 2);
	else if (code == 4)
		ft_putendl_fd(": could not execute binary", 2);
	return (127);
}

int		ft_setenv_error(int code)
{
	if (code == 1)
		ft_putendl_fd("setenv: Too many arguments", 2);
	else if (code == 2)
		ft_putendl_fd("setenv: Variable name must begin with a letter", 2);
	else
		ft_putendl_fd("setenv: Variable name must \
contain alphanumeric characters", 2);
	return (-1);
}

int		ft_env_opt_err(char opt)
{
	ft_putstr_fd("env: illegal option -- ", 2);
	ft_putchar_fd(opt, 2);
	ft_putendl_fd("\nusage: env [-i] [name=value ...] \
[utility [argument ...]]", 2);
	return (-1);
}
