/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/07 13:12:46 by lsimonne          #+#    #+#             */
/*   Updated: 2016/03/25 16:06:15 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHELL_H

# define SHELL_H

# include "libft.h"
# include "get_next_line.h"
# include <stdlib.h>
# include <sys/types.h>
# include <sys/wait.h>
# include <stdbool.h>

typedef struct		s_command
{
	char			*path;
	char			*name;
	char			**av;
	int				ac;
}					t_command;

typedef struct		s_env
{
	char			*var;
	char			*value;
	struct s_env	*next;
}					t_env;

typedef struct		s_env_opt
{
	bool	i;
}					t_env_opt;

typedef struct		s_cd_opt
{
	bool	p;
	bool	l;
}					t_cd_opt;

/*
**  minishell.c
*/

int					ft_prompt(t_env *env);
int					ft_command(char *input, t_env *env, t_env *e_copy);

/*
**  command.c
*/

int					ft_get_cmd(char *tmp, t_command *cmd, t_env *env);
char				**ft_get_var(t_env *env, const char *var);

/*
** builtins.c
*/

void				ft_builtins(t_command cmd, int code, t_env *env);

/*
** env.c
*/

void				ft_add_var(char *elem, t_env **begin);
char				**ft_env_array(t_env *env, t_env *e_tmp);
int					ft_env(t_env *env, t_command cmd);
char				*ft_join_args(char **args, int i);

/*
** env2.c
*/

int					ft_setenv(t_env *env, t_command cmd);
int					ft_replace_var(t_env *env, char *var, char *value);
void				ft_unsetenv(t_env *env, t_command cmd);

/*
** env3.c
*/

void				ft_print_env(t_env *env);
void				ft_set_var(t_env *env, char **var);
t_env				*ft_getenv(char **envp);
void				ft_sub_env(t_command cmd, int j, t_env *e_copy, t_env *env);

/*
** errrors.c
*/

int					ft_wrong_command(int code, t_command cmd);
int					ft_arg_error(int code, char *path);
int					ft_env_opt_err(char opt);
int					ft_cd_error(int code, char *path);
int					ft_setenv_error(int code);

/*
** cd.c
*/

int					ft_cd(t_command cmd, t_env *env);
char				*ft_get_value(t_env *env, char *var);

/*
** cd2.c
*/

char				*ft_dot_dot(char *path, char *pwd);
int					ft_after_options(t_command cmd, int dash);
void				ft_replace_pwds(t_env *env, char *cwd, char *path, \
					t_cd_opt opt);

/*
** signals.c
*/

void				ft_signals(int signal);

char				**ft_strshellsplit(char *s);

#endif
