/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_array.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/18 13:00:49 by lsimonne          #+#    #+#             */
/*   Updated: 2016/03/21 17:14:39 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	ft_free_array(char ***array)
{
	int	i;

	i = 0;
	while (*array[i])
	{
		ft_strdel(&(*array[i]));
		i++;
	}
	free(*array);
	*array = NULL;
}
