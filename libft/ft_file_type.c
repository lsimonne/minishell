/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_file_type.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/16 10:27:40 by lsimonne          #+#    #+#             */
/*   Updated: 2016/03/17 18:08:37 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <sys/stat.h>

static char	ft_get_type(mode_t mode)
{
	char	type;

	if (S_ISLNK(mode))
		type = 'l';
	else if (mode & S_IFREG)
		type = '-';
	else if (S_ISBLK(mode))
		type = 'b';
	else if (mode & S_IFDIR)
		type = 'd';
	else if (S_ISCHR(mode))
		type = 'c';
	else
		type = 'p';
	if (S_ISSOCK(mode))
		type = 's';
	return (type);
}

char		ft_file_type_l(char *path)
{
	struct stat		stats;

	if (stat(path, &stats) == -1)
		return (-1);
	else
		return (ft_get_type(stats.st_mode));
}

char		ft_file_type(char *path)
{
	struct stat	stats;

	if (lstat(path, &stats) == -1)
		return (-1);
	else
		return (ft_get_type(stats.st_mode));
}
