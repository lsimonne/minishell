NAME = minishell

HEADER = includes/shell.h

FLAGS = -Wall -Werror -Wextra

LIB = libft

SRC = main.c \
	  minishell.c \
	  command.c \
	  builtins.c \
	  env.c \
	  env2.c \
	  env3.c \
	  errors.c \
	  cd.c \
	  cd2.c \
	  signals.c \
	  split.c

OBJ = $(SRC:%.c=obj/%.o)

all: prep compile_lib $(NAME)

prep:
	mkdir -p obj/

compile_lib: 
			make -C $(LIB)/

$(NAME): $(OBJ)
		gcc $(FLAGS) -o $(NAME) $(OBJ) -L $(LIB)/ -lft

obj/%.o: %.c $(HEADER)
		gcc $(FLAGS) -I $(LIB)/includes -I includes/ -o $@ -c $<

clean:
		make -C $(LIB)/ fclean
		rm -rf -- obj/

fclean: clean
		rm -f $(NAME)
		make -C $(LIB)/ fclean

re: fclean all
