/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/10 11:13:47 by lsimonne          #+#    #+#             */
/*   Updated: 2016/03/25 16:12:57 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

t_env	*ft_env_copy(t_env *env)
{
	t_env	*copy;
	char	*elem;
	char	*tmp;

	copy = NULL;
	while (env)
	{
		tmp = ft_strjoin(env->var, "=");
		if (env->value)
			elem = ft_strjoin(tmp, env->value);
		else
			elem = ft_strdup(tmp);
		ft_strdel(&tmp);
		ft_add_var(elem, &copy);
		ft_strdel(&elem);
		env = env->next;
	}
	return (copy);
}

int		ft_parse_options(char **args, t_env_opt *options)
{
	int		i;
	int		j;

	i = 1;
	while (args[i] && *args[i] == '-')
	{
		j = 1;
		if (args[i][j] == '-')
			break ;
		while (args[i][j])
		{
			if (args[i][j] == 'i')
				options->i = 1;
			else
				return (ft_env_opt_err(args[i][j]));
			j++;
		}
		i++;
	}
	return (0);
}

char	*ft_join_args(char **args, int i)
{
	char	*tmp;
	char	*tmp2;

	if (!(args[i + 1]))
		return (args[i]);
	tmp = ft_strdup(args[i]);
	while (args[i + 1])
	{
		tmp2 = ft_strjoin(tmp, " ");
		i++;
		ft_strdel(&tmp);
		tmp = ft_strjoin(tmp2, args[i]);
		ft_strdel(&tmp2);
	}
	return (tmp);
}

int		ft_env(t_env *env, t_command cmd)
{
	int			j;
	t_env		*e_copy;
	t_env_opt	opt;

	j = 1;
	e_copy = NULL;
	opt.i = 0;
	if (cmd.ac == 1)
		ft_print_env(env);
	else
	{
		if (ft_parse_options(cmd.av, &opt) == -1)
			return (-1);
		if (opt.i == 0)
			e_copy = ft_env_copy(env);
		else
			ft_add_var("BUILTINS=exit:env:setenv:unsetenv:cd", &e_copy);
		while (cmd.av[j] && *cmd.av[j] && *cmd.av[j] == '-' \
				&& (!cmd.av[j][1] || cmd.av[j][1] != '-'))
			j++;
		ft_sub_env(cmd, j, e_copy, env);
	}
	return (0);
}

char	**ft_env_array(t_env *env, t_env *e_tmp)
{
	char	**envp;
	char	*tmp2;
	int		size;

	size = 0;
	while (e_tmp)
	{
		size++;
		e_tmp = e_tmp->next;
	}
	if (!(envp = (char **)malloc(sizeof(char *) * (size + 1))))
		return (NULL);
	envp[size] = NULL;
	size = 0;
	while (env)
	{
		tmp2 = ft_strjoin(env->var, "=");
		if (env->value)
			envp[size++] = ft_strjoin(tmp2, env->value);
		else
			envp[size++] = ft_strdup(tmp2);
		ft_strdel(&tmp2);
		env = env->next;
	}
	return (envp);
}
