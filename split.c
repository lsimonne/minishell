/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/23 18:11:31 by lsimonne          #+#    #+#             */
/*   Updated: 2016/03/24 21:02:21 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static size_t	ft_word_number(const char *str)
{
	size_t		size;
	const char	*tmp;

	size = 0;
	tmp = str;
	while (*tmp)
	{
		while (*tmp && (*tmp == ' ' || *tmp == '\t'))
			tmp++;
		if (*tmp && *tmp != ' ' && *tmp != '\t')
		{
			size++;
			tmp++;
		}
		while (*tmp && *tmp != ' ' && *tmp != '\t')
			tmp++;
	}
	return (size);
}

static size_t	ft_word_length(char *s)
{
	size_t len;

	len = 0;
	while (s[len] && s[len] != ' ' && s[len] != '\t')
		len++;
	return (len);
}

static char		*ft_dup_word(char *start, size_t len)
{
	char	*str;
	size_t	i;

	i = 0;
	if (!(str = (char *)malloc(sizeof(char) * (len + 1))))
		return (NULL);
	while (i < len)
	{
		str[i] = start[i];
		i++;
	}
	str[i] = '\0';
	return (str);
}

char			**ft_strshellsplit(char const *s)
{
	size_t	size;
	size_t	i;
	size_t	len;
	char	**array;
	char	*tmp;

	i = 0;
	if (s == NULL)
		return (NULL);
	size = ft_word_number(s);
	tmp = (char *)s;
	if (!(array = (char **)malloc(sizeof(char *) * (size + 1))))
		return (NULL);
	while (i < size)
	{
		while (*tmp && (*tmp == ' ' || *tmp == '\t'))
			tmp++;
		len = ft_word_length(tmp);
		array[i] = ft_dup_word(tmp, len);
		i++;
		tmp = tmp + len;
	}
	array[i] = NULL;
	return (array);
}
