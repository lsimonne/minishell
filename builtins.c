/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/15 13:36:14 by lsimonne          #+#    #+#             */
/*   Updated: 2016/03/24 16:28:27 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

static void	ft_free_env(t_env *env)
{
	t_env *tmp;

	if (env)
	{
		ft_strdel(&env->var);
		if (env->value)
			ft_strdel(&env->value);
		tmp = env->next;
		free(env);
		ft_free_env(tmp);
	}
}

static int	ft_exit(t_command cmd, t_env *env)
{
	int	i;

	i = 0;
	ft_free_env(env);
	if (cmd.ac > 2)
	{
		ft_putendl_fd("exit: Too many arguments", 2);
		return (-1);
	}
	else if (cmd.ac == 2)
	{
		while (cmd.av[1][i])
		{
			if (ft_isdigit(cmd.av[1][i]) == 0 && cmd.av[1][i] != '-')
			{
				ft_putstr_fd("minishell: exit: ", 2);
				ft_putstr_fd(cmd.av[1], 2);
				ft_putendl_fd(": numeric argument required", 2);
				exit(255);
			}
			i++;
		}
		exit(ft_atoi(cmd.av[1]));
	}
	exit(0);
}

void		ft_builtins(t_command cmd, int code, t_env *env)
{
	if (code == 2)
		ft_exit(cmd, env);
	else if (code == 3)
		ft_env(env, cmd);
	else if (code == 4)
		ft_setenv(env, cmd);
	else if (code == 5)
		ft_unsetenv(env, cmd);
	else
		ft_cd(cmd, env);
}
