/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/16 11:29:35 by lsimonne          #+#    #+#             */
/*   Updated: 2016/03/25 16:12:59 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"
#include <signal.h>
#include <sys/utsname.h>

int			ft_command(char *input, t_env *env, t_env *e_copy)
{
	int			i;
	pid_t		process;
	t_command	cmd;

	i = 0;
	process = -1;
	if ((i = ft_get_cmd(input, &cmd, env)) < 0)
		ft_wrong_command(i, cmd);
	else if (i > 1)
		ft_builtins(cmd, i, e_copy);
	else
		process = fork();
	if (process > 0)
	{
		signal(SIGINT, SIG_IGN);
		wait(0);
	}
	if (process == 0)
	{
		if (i == 1)
			cmd.path = ft_strdup(cmd.av[0]);
		execve(cmd.path, cmd.av, ft_env_array(e_copy, e_copy));
	}
	return (0);
}

static char	*ft_get_user(t_env *env, char *tmp)
{
	char	*user;
	char	*prompt;

	user = NULL;
	while (env && ft_strcmp(env->var, "USER") != 0)
		env = env->next;
	if (env && env->value)
		user = ft_strdup(env->value);
	if (user)
	{
		prompt = ft_strjoin(tmp, user);
		ft_strdel(&user);
	}
	else
		prompt = ft_strjoin(tmp, "anonymous");
	return (prompt);
}

static void	ft_display_prompt(t_env *env)
{
	char			*prompt;
	char			*tmp;
	int				len;
	struct utsname	machine;

	if (uname(&machine) != -1)
	{
		tmp = ft_strjoin("[", machine.nodename);
		len = ft_strchr(tmp, '.') - tmp;
		prompt = ft_strsub(tmp, 0, len);
		ft_strdel(&tmp);
		tmp = ft_strjoin(prompt, " - ");
		ft_strdel(&prompt);
		prompt = ft_get_user(env, tmp);
		ft_strdel(&tmp);
		tmp = ft_strjoin(prompt, "]  $> ");
		ft_strdel(&prompt);
		ft_putstr(tmp);
		ft_strdel(&tmp);
	}
}

int			ft_prompt(t_env *env)
{
	char		*tmp;
	int			i;
	char		**input;
	void		(*sig)(int);

	sig = ft_signals;
	while (1)
	{
		signal(SIGINT, sig);
		ft_display_prompt(env);
		if (get_next_line(0, &tmp) > 0 && ft_strcmp(tmp, "") != 0)
		{
			i = 0;
			input = ft_strsplit(tmp, ';');
			ft_strdel(&tmp);
			while (input[i])
			{
				ft_command(input[i], env, env);
				i++;
			}
		}
	}
	return (0);
}
