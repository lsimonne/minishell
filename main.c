/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/07 16:49:20 by lsimonne          #+#    #+#             */
/*   Updated: 2016/03/24 19:32:51 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"
#include <fcntl.h>

int		main(int ac, char **av, char **envp)
{
	t_env	*env;
	char	*cwd;
	char	*tmp;

	if (av[ac - 1])
	{
		env = ft_getenv(envp);
		if (!(ft_get_value(env, "PWD")))
		{
			tmp = getcwd(NULL, 0);
			if (ft_replace_var(env, "PWD", tmp) == 0)
			{
				cwd = ft_strjoin("PWD=", tmp);
				ft_strdel(&tmp);
				ft_add_var(cwd, &env);
				ft_strdel(&cwd);
			}
		}
		ft_prompt(env);
	}
	return (0);
}
