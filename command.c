/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/07 16:15:35 by lsimonne          #+#    #+#             */
/*   Updated: 2016/03/25 14:20:11 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell.h>
#include <dirent.h>

char		**ft_get_var(t_env *env, const char *var)
{
	char	**values;

	while (env)
	{
		if (ft_strcmp(env->var, var) == 0)
		{
			values = ft_strsplit(env->value, ':');
			return (values);
		}
		env = env->next;
	}
	return (NULL);
}

int			ft_check_path(t_command *cmd, t_env *env, char **paths)
{
	int				i;
	DIR				*dirp;
	struct dirent	*dirnt;
	char			*tmp;

	i = 0;
	if (!(paths = ft_get_var(env, "PATH")))
		return (-1);
	while (paths[i])
	{
		if (!(dirp = opendir(paths[i])))
			return (-1);
		while ((dirnt = readdir(dirp)) != NULL)
		{
			if (cmd->name && ft_strcmp(dirnt->d_name, cmd->name) == 0)
			{
				tmp = ft_strjoin(paths[i], "/");
				cmd->path = ft_strjoin(tmp, cmd->name);
				ft_strdel(&tmp);
				return (0);
			}
		}
		i++;
	}
	return (-1);
}

int			ft_check_builtins(t_command *cmd, t_env *env)
{
	char	**builtins;
	int		code;

	code = 2;
	builtins = ft_get_var(env, "BUILTINS");
	while (builtins && *builtins)
	{
		if (cmd->name && ft_strcmp(*builtins, cmd->name) == 0)
			return (code);
		builtins++;
		code++;
	}
	return (0);
}

static void	ft_replace_by_env(char **str, t_env *env)
{
	while (env && (*str + 1) && ft_strcmp(env->var, *str + 1) != 0)
		env = env->next;
	if (env)
	{
		ft_strdel(str);
		*str = ft_strdup(env->value);
	}
}

int			ft_get_cmd(char *input, t_command *cmd, t_env *env)
{
	int		code;
	char	*paths;

	cmd->path = NULL;
	cmd->ac = 0;
	cmd->av = ft_strshellsplit(input);
	if (*cmd->av)
	{
		while (cmd->av[cmd->ac])
		{
			if (*cmd->av[cmd->ac] == '$')
				ft_replace_by_env(&cmd->av[cmd->ac], env);
			cmd->ac++;
		}
		cmd->name = ft_strdup(cmd->av[0]);
		if ((code = ft_check_builtins(cmd, env)) > 0)
			return (code);
		if (ft_check_path(cmd, env, &paths) == 0)
			return (0);
		if (access(cmd->av[0], X_OK) == 0)
			return (1);
		if (access(cmd->av[0], F_OK) == 0)
			return (-2);
	}
	return (-1);
}
