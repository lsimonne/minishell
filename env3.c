/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env3.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/18 10:41:44 by lsimonne          #+#    #+#             */
/*   Updated: 2016/03/25 16:04:52 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void	ft_print_env(t_env *env)
{
	t_env	*tmp;

	tmp = env->next;
	while (tmp)
	{
		ft_putstr(tmp->var);
		ft_putchar('=');
		if (tmp->value)
			ft_putendl(tmp->value);
		else
			ft_putchar('\n');
		tmp = tmp->next;
	}
}

t_env	*ft_getenv(char **envp)
{
	t_env	*env;

	env = NULL;
	ft_add_var("BUILTINS=exit:env:setenv:unsetenv:cd", &env);
	while (*envp)
	{
		ft_add_var(*envp, &env);
		envp++;
	}
	return (env);
}

void	ft_set_var(t_env *env, char **var)
{
	char	*elem;
	char	*tmp;

	if (ft_replace_var(env, var[1], var[2]) == 0)
	{
		tmp = ft_strjoin(var[1], "=");
		if (var[2])
		{
			elem = ft_strjoin(tmp, var[2]);
			ft_add_var(elem, &env);
			ft_strdel(&elem);
		}
		else
			ft_add_var(tmp, &env);
		ft_strdel(&tmp);
	}
}

void	ft_sub_env(t_command cmd, int j, t_env *e_copy, t_env *env)
{
	char	*str;
	char	**tmp;

	while (cmd.av[j] && ft_strchr(cmd.av[j], '=') != NULL)
	{
		tmp = ft_strsplit(cmd.av[j], '=');
		if (ft_replace_var(e_copy, tmp[0], tmp[1]) == 0)
			ft_add_var(cmd.av[j], &e_copy);
		j++;
	}
	if (cmd.av[j])
	{
		str = ft_join_args(cmd.av, j);
		ft_command(str, env, e_copy);
	}
	else
		ft_print_env(e_copy);
}
